/*
Maintain an array of ten positive numbers. Initially all elements are set to zero 
(indicating array is empty). Write a menu driven program to perform operations like 
add number, delete number, find maximum number (along with its index),find minimum number 
(along with its index) and sum of numbers.While adding number display available indexes
and user can select any of them.If no index is free, display appropriate message. 
Also, while deleting number display available indexes along with values and user
can clear value there (set to zero).
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

enum OPERATIONS{EXIT,ADD,DELETE,MAXIMUM,MINIMUM,SUM,DISPLAY};
void add(int *arr);
void display(int *arr);
void delete(int *arr);              // function prototype....
void sum (int *arr);
void maximum(int *arr);
void minimum(int *arr);

enum OPERATIONS choice;

int main(void)
{
    int choice;
    int arr[10];
    int i=0;
    int j=0;
    for(i=0;i<10;i++)
    {
        arr[i]=0;
    }
    do
    {
        //printf("Enter the choice\n");
        printf("0.EXIT\n");
        printf("1.ADD NUMBER\n");
        printf("2.DELETE NUMMBER\n");
        printf("3.MAXIMUM NUMBER\n");
        printf("4.MINIMUM NUMBER\n");
        printf("5.SUM OF ALL NUMBER\n");
        printf("6.DISPLAY ELENEMTS\n");
        printf("Enter choice : ");
        scanf("%d",&choice);
        switch(choice)
        {
                case ADD :
                    add(arr);
                    break;

                case DELETE :
                    delete(arr);
                    break;

                case MAXIMUM :
                    maximum(arr);
                    break;

                case MINIMUM :
                    minimum(arr);
                    break;

                case SUM :
                    sum(arr);
                    break;
                
                case DISPLAY :
                    display(arr);
                    break;
            }

    } while(choice != EXIT);
}
void add(int *arr)
{
    int i=0;
    int number;
    printf("In which index you want add number : arr ");
    scanf("%d",&i);
    if (arr[i]==0)
    {
        printf("Enter the number in arr[%d] : " ,i);
        scanf("%d",&number);
        arr[i]=number;
        printf("number that you enter in arr[%d] is %d \n",i,number);
    }
    else
    printf("index in arr[%d] is not empty\nto add the number first delete arr[%d]\n",i,i);
}

void display(int *arr)
{
    int i=0;
    int j=0;
    for( i=0;i<10;i++)
    {
        printf("array index [%d] : %d \n",j,arr[i]);
        j++;
    }
}

void delete(int *arr)
{
    //arr [1]=0;
    int i=0;
    int number;
    printf("In which index you want delete number : arr ");
    scanf("%d",&i);
    arr[i]=0;
    printf("Number in arr[%d] deleted in deleted successfully.... \n",i);   
}

void sum (int *arr)
{
    int i;
    int ans=0 ;
    for (i=0;i<10;i++)
    {
    ans = ans+arr[i];
    }
    printf("Sum of all array elments = %d \n",ans); 
}

void maximum(int *arr)
{
    printf("\n");
    int i,max;
    max=arr[0];
    for (i=0;i<10;i++)
    {
        if (max > arr[i])
        { 
          ;
        }
        else
        {
            max = arr [i];
        }
    }
    //printf("Maximum number = %d\n",max);
    for (i=0;i<10;i++)
    {
    if(max == arr[i])
        printf("Maximum number = %d having index arr[%d]\n",max,i);    
    }
}

void minimum(int *arr)
{
    int i,min;
    for (i=0;i<10;i++)
    {
    if (min < arr[i])
    {
            ;
    }
    else
    {
        min = arr [i];
    }
    }
    //printf("Minimum number = %d\n",max);
     for (i=0;i<10;i++)
    {
    if(min == arr[i])
        printf("Minimum number = %d having index arr[%d]\n",min,i);    
    }
}