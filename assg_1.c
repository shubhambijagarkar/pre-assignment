/*
Input a string from user on command line. 
String may have multiple commas e.g. "Welcome,to,Sunbeam,CDAC,Diploma,Course". 
Print each word individually. Hint: use strtok() function.
*/

#include<stdio.h>  
#include<string.h>

int main(int argc,char *argv[])
{

    char *token = strtok(argv[1] , ",");
    while ( token != NULL)
    {
        printf(" %s\n",token);
        token = strtok (NULL , ",");
        //printf("count : %d",argc);
    }
    return 0;
}