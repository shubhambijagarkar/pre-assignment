/*
8. A CSV file contains movies data in given form. 
Read the file and load data into a singly linked list of movie objects. 
Also give facility to find movies by name and by genre.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MOVIE_DB	"movies.csv"

typedef struct movie {
	int id;
	char name[80];
	char genres[220];
}movie_t;

void movie_display(movie_t *m) {
	printf("id=%d, name=%s, genres=%s\n", m->id, m->name, m->genres);
}

typedef struct node {
	movie_t data;
	struct node *next;
}node_t;

node_t *head = NULL; // address of the first node of list (initially NULL) -- global

node_t* create_node(movie_t val) {
	node_t *newnode = (node_t*)malloc(sizeof(node_t));
	newnode->data = val;
	newnode->next = NULL;
	return newnode;
}

void add_last(movie_t val) {
	node_t *newnode = create_node(val);
	if(head == NULL)
		head = newnode;
	else {
		node_t *trav = head;
		while(trav->next != NULL) {
			trav = trav->next;
		}
		trav->next = newnode;
	}
}

void display_list() {
	node_t *trav = head;
	while(trav != NULL) {
		movie_display(&trav->data);
		trav = trav->next;
	}
}

int parse_movie(char line[], movie_t *m) {
	int success = 0;
	char *id, *name, *genres;
	id = strtok(line, ",\n");
	name = strtok(NULL, ",\n");
	genres = strtok(NULL, ",\n");
	if(id == NULL || name == NULL || genres == NULL)
		success = 0; // partial record
	else {
		success = 1;
		m->id = atoi(id);	// "10" (string) --> 10 (int)
		strcpy(m->name, name);
		strcpy(m->genres, genres);
	}
	return success;
}

void load_movies() {
	FILE *fp;
	char line[1024];
	movie_t m;
	fp = fopen(MOVIE_DB, "r");
	if(fp == NULL) {
		perror("failed to open movies file");
		exit(1);
	}

	while(fgets(line, sizeof(line), fp) != NULL) {
		//printf("%s\n", line);
		parse_movie(line, &m);
		//movie_display(&m);
		add_last(m); // add movie object into the linked list
	}

	fclose(fp);
}

void find_movie_by_name() {
	char name[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie name to be searched: ");
	gets(name);

	trav = head;
	while(trav != NULL) {
		if(strstr(name, trav->data.name) == 0) {
			movie_display(&trav->data);
			found = 1;
			break;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}

void find_movie_by_genre() {
	char genre[80];
	node_t *trav = head;
	int found = 0;
	printf("enter movie genre to be searched: ");
	gets(genre);

	trav = head;
	while(trav != NULL) {
		if(strstr(trav->data.genres, genre) != NULL) {
			movie_display(&trav->data);
			found = 1;
		}
		trav = trav->next;
	}

	if(!found)
		printf("movie not found.\n");
}

int main() {
	load_movies();
	display_list(); // diplay movie linked list
	find_movie_by_name();
	find_movie_by_genre();
	return 0;
}

