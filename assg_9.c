/*
9. An inventory management system needs to manage data of the items into binary file. 
Each item has id, name, price and quantity. 
Write a menu-driven program to implement add, find, display all, edit and delete operations from the items file. 
Order id (int) should be generated automatically and must be unique.
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ITEMS_DB "items.db"
#define TEMP_DB "temp.db"

typedef struct item
{
    int id ;
    char name [50];
    double price;
    int quantity;
}item_t;

void add_item();
void accept_item(item_t *i);
int auto_gen_next_id();
void display_items();
void display (item_t *i);
void find();
void edit();
void delete();

int main ()
{
    int choice;
    do{
        printf("\n1. Add items\n2. Find items by name\n3. Display Items\n4. Edit items\n5. Delete items\nEnter your choice : ");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1 :
                add_item();
                break;
            case 2 :
                find();
                break;
            case 3 :
                display_items();
                break;
            case 4 :
                edit();
                break;
            case 5 :
                delete();
                break;
        }
    }while(choice != 0);
    return 0;
}

void delete()
{
    int id, found=0;
    item_t i;
    printf("Enter item ID which you wanna delete : ");
    scanf("%d",&id);
    FILE *fp;
    FILE *fptr;
    fp=fopen(ITEMS_DB,"rb");
    fptr=fopen(TEMP_DB,"wb");
    if(fp==NULL)
    {
        printf("failed to open items file");
        return;
    }
    if(fptr==NULL)
    {
        printf("failed to open temp file");
        return;
    }
    while(fread(&i,sizeof(item_t),1,fp)>0)
    {
        if(id==i.id)
        {
            found = 1;
            printf("Item deleted successfuly...\n");
        }
        else
            fwrite(&i, sizeof(item_t),1,fptr);
      
    }
    if(!found)
        printf("\n No such item found \n");
    fclose(fp);
    fclose(fptr);
    remove(ITEMS_DB);
    rename(TEMP_DB,ITEMS_DB);
}

void edit()
{
    FILE *fp;
    item_t i;
    int found = 0;
    char name [40];
    long size  = sizeof(item_t);
    printf("Enter the name of item you wanna edit : ");
    scanf("%s",name);
    fp = fopen(ITEMS_DB, "rb+");
    if (fp == NULL)
    {
        perror("cannot open item,db file");
        return;
    }
    while(fread(&i, size, 1, fp) > 0)
    {
        if (strcmp(name , i.name) == 0)
        {
            found = 1;
            break;
        }
    }
    if (found)
    {
        item_t ni;
        FILE *fptr;
        ni.id = i.id;
        accept_item(&ni);
        fseek(fp, -size, SEEK_CUR);
        fwrite(&ni, size, 1, fp);
        printf("changes are saved successfully...\n");
    }
    if(!found)
        printf("item is not found\n");
    fclose(fp);
    return;
}

void find()
{
    FILE *fp;
    item_t i;
    int found = 0;
    long size = sizeof(item_t);
    char name [40];
    printf("Enter the name of item you wanna find : ");
    scanf("%s",name);
    fp = fopen(ITEMS_DB, "rb");
    if (fp == NULL)
    {
        perror("cannot open items,db file");
        return;
    }
    while(fread(&i, size , 1, fp)>0)
    {
        if (strcmp(name ,i.name) == 0)
        {
            found = 1;
            display(&i);
        }
    }
    if (!found)
        printf("Item is not available.\n");
    fclose(fp);
    return;
}

void accept_item(item_t *i)
{
    //printf("Enter id : ");
    //scanf("%d",&i->id);
    printf("Enter name : ");
    scanf("%s",i->name);
    printf("Enter price : ");
    scanf("%lf",&i->price);
    printf("Enter quantity : ");
    scanf("%d",&i->quantity);
}

int auto_gen_next_id()
{
    FILE *fp;
    long size = sizeof(item_t);
    int max =0;
    item_t i;
    fp = fopen(ITEMS_DB, "rb");
    if(fp == NULL)
    {
        return max + 1;
    }
    fseek(fp,-size,SEEK_END);
    if(fread(&i, size, 1 ,fp)>0)
        max = i.id;
    fclose(fp);
    return max + 1;
}

void add_item()
{
    item_t i;
    FILE *fp;
    accept_item(&i);
    i.id = auto_gen_next_id();
    // open the file
    fp = fopen(ITEMS_DB,"ab");
    if (fp ==NULL)
    {
        perror(" cannot open item.db file.");
        return;
    }
    fwrite(&i,sizeof(item_t), 1 ,fp);
    fclose(fp);
    return;
}

void display (item_t *i)
{
    printf("id: %d,\tname: %s,\tprice: %.2lf,\tquantity: %d\n",i->id,i->name,i->price,i->quantity);
}

void display_items()
{
    FILE *fp;
    item_t i;
    long size  = sizeof(item_t);
    fp = fopen(ITEMS_DB, "rb");
    if (fp==NULL)
    {
        perror("cannot open item.db file.");
        return;
    }
    while(fread(&i, size ,1, fp)>0)
    {
        display(&i);
    }
    fseek(fp , -size, SEEK_CUR);
    if(fread(&i, size ,1, fp)==0)
        printf("Invertory is empty...\n");
    
    fclose(fp);
    return;
}